<?php 
if (!defined('BASEPATH'))
exit('No direct script access allowed');
  
class Details extends CI_Controller {
  
    function __construct() {
        parent::__construct();
        $this->load->model('Main_model');
    }


    function index($seo) {
    	$actual_link = base_url() . KEY_WORD . $seo;
      	$fb = $actual_link;
      	$tw = "El mejor catálogo de escorts en Bogotá, fotos 100% reales con servicio garantizado. ";
      	$wa = "El mejor catálogo de escorts en Bogotá, fotos 100% reales con servicio garantizado. " . $actual_link;

      	$id = $this->Main_model->getIdBySeo($seo);
      	$this->Main_model->update_view($id);

		$data = $this->Main_model->get_user_profile($id);
  		$userid = $data->userid;
  		$week =  $this->Main_model->sqlGetWeekSchedule($userid);
  		$weekday = array();
  		$key = 0;
  		
		foreach ($week as $value) {
			$weekday[$key] = new StdClass;
		    $weekday[$key]->day = $this->Main_model->explodeTimeslot($value);
		    $weekday[$key]->dayinSpanish = $this->Main_model->GetDayInSpanish(date('l', strtotime("+".$key." days")));
		    $key++;
		}

  		$last_update = $this->Main_model->sqlLastAgendaUpdate($userid);
		    
  		$images = explode(",",$data->pictures);
  		$i = 0;
      	$this->load->view('details', array(	'actual_link'	=> $actual_link,
      										'images'		=> $images, 
      										'i'				=> $i,
      										'data'			=> $data,
      										'tw'			=> $tw,
      										'wa'			=> $wa,
      										'fb'			=> $fb,
      										'last_update' 	=> $last_update,
      										'weekday' 		=> $weekday));
    }


}