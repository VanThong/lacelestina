<?php 
if (!defined('BASEPATH'))
exit('No direct script access allowed');
  
class Welcome extends CI_Controller {
  
    function __construct() {
        parent::__construct();
        $this->load->model('Welcome_model');
    }
    public $unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', ' '=>'-', ','=>'_' );

    public $US_STATES = array(
	    'AL'=>'Alabama',
	    'AK'=>'Alaska',
	    'AZ'=>'Arizona',
	    'AR'=>'Arkansas',
	    'CA'=>'California',
	    'CO'=>'Colorado',
	    'CT'=>'Connecticut',
	    'DE'=>'Delaware',
	    'DC'=>'District of Columbia',
	    'FL'=>'Florida',
	    'GA'=>'Georgia',
	    'HI'=>'Hawaii',
	    'ID'=>'Idaho',
	    'IL'=>'Illinois',
	    'IN'=>'Indiana',
	    'IA'=>'Iowa',
	    'KS'=>'Kansas',
	    'KY'=>'Kentucky',
	    'LA'=>'Louisiana',
	    'ME'=>'Maine',
	    'MD'=>'Maryland',
	    'MA'=>'Massachusetts',
	    'MI'=>'Michigan',
	    'MN'=>'Minnesota',
	    'MS'=>'Mississippi',
	    'MO'=>'Missouri',
	    'MT'=>'Montana',
	    'NE'=>'Nebraska',
	    'NV'=>'Nevada',
	    'NH'=>'New Hampshire',
	    'NJ'=>'New Jersey',
	    'NM'=>'New Mexico',
	    'NY'=>'New York',
	    'NC'=>'North Carolina',
	    'ND'=>'North Dakota',
	    'OH'=>'Ohio',
	    'OK'=>'Oklahoma',
	    'OR'=>'Oregon',
	    'PA'=>'Pennsylvania',
	    'RI'=>'Rhode Island',
	    'SC'=>'South Carolina',
	    'SD'=>'South Dakota',
	    'TN'=>'Tennessee',
	    'TX'=>'Texas',
	    'UT'=>'Utah',
	    'VT'=>'Vermont',
	    'VA'=>'Virginia',
	    'WA'=>'Washington',
	    'WV'=>'West Virginia',
	    'WI'=>'Wisconsin',
	    'WY'=>'Wyoming',
	);

    function index() {
    	ini_set('display_startup_errors',1);
		ini_set('display_errors',1);
		error_reporting(-1);
		$fb = "https://www.lacelestina.co";
		$tw = "El mejor catálogo de escorts en Bogotá y Medellin, fotos 100% reales con servicio garantizado ";
		$wa = "El%20mejor%20catálogo%20de%20escorts%20en%20Bogotá,%20fotos%20100%%20reales%20con%20servicio%20garantizado%20https://www.lacelestina.co";
		$filter = [
		    "sex" => "",
		    "sexuality" => "",
		    "occupation" => "",
		    "bust_size" => "",
		    "but_size" => "",
		    "rate" => "",
		    "age1" => "",
		    "age2" => "",
		    "figure" => "",
		    "amount1" => "",
		    "amount2" => "",
		    "eyes_color" => "",
		    "hair_color" => ""
		];

		if ($this->Welcome_model->isMobile()){
		    $limit = NUMBER_OF_PROFILES_ONLOAD_MOBILE;
		} else $limit = NUMBER_OF_PROFILES_ONLOAD_DESKTOP;
		  
		$offset = 0;
		$this->session->set_userdata('where', '');

		if ($this->input->post() !== null){
		    if(count($this->input->post())){
		        $str = "";
		        //Color de piel
		        if ( ($this->input->post('checkbox_figure') !== null) && $this->input->post('checkbox_figure') != '') {
		            $figure = "'" . implode("', '", $this->input->post('checkbox_figure')) . "'";
		            $str.= " AND ( figure IN ($figure) )";
		            $filter["figure"] = $this->input->post('checkbox_figure');
		        }
		        //Edad
		        if ( ($this->input->post('age1') !== null) && $this->input->post('age1') != '' && ($this->input->post('age2') !== null) && $this->input->post('age2') != '') {
		            $str.= " AND ( age BETWEEN " . $this->input->post('age1') . "  AND  " . $this->input->post('age2') . " )";
		            $filter["age1"] = $this->input->post('age1');
		            $filter["age2"] = $this->input->post('age2');
		        }
		        //Rango de precios
		        if ( ($this->input->post('amount1') !== null) && $this->input->post('amount1') != '' && ($this->input->post('amount2') !== null) && $this->input->post('amount2') != '') {
		            $str.= " AND ( rate BETWEEN " . $this->input->post('amount1') . "  AND  " . $this->input->post('amount2') . " )";
		            $filter["amount1"] = $this->input->post('amount1');
		            $filter["amount2"] = $this->input->post('amount2');
		        }
		        
		        //Color de pelo
		        if ( ($this->input->post('checkbox_hair_color') !== null) && $this->input->post('checkbox_hair_color') != '') {
		            $hair_color = "'" . implode("', '", $this->input->post('checkbox_hair_color')) . "'";
		            $str.= " AND ( hair_color IN ($hair_color) )";
		            $filter["hair_color"] = $this->input->post('checkbox_hair_color');
		        }
		        //Contextura
		        if ( ($this->input->post('checkbox_cola_size') !== null) && $this->input->post('checkbox_cola_size') != '') {
		            $cola_size = "'" . implode("', '", $this->input->post('checkbox_cola_size')) . "'";
		            $str.= " AND ( occupation IN ($cola_size) )";
		            $filter["occupation"] = $this->input->post('checkbox_cola_size');
		        }
		        //Contextura
		        if ( ($this->input->post('checkbox_but_size') !== null) && $this->input->post('checkbox_but_size') != '') {
		            $but_size = "'" . implode("', '", $this->input->post('checkbox_but_size')) . "'";
		            $str.= " AND ( But_Size IN ($but_size) )";
		            $filter["but_size"] = $this->input->post('checkbox_but_size');
		        }
		        //Tamaño del busto
		        if ( ($this->input->post('checkbox_bust_size') !== null) && $this->input->post('checkbox_bust_size') != '') {
		            $bust_size = "'" . implode("', '", $this->input->post('checkbox_bust_size')) . "'";
		            $str.= " AND ( Bust_Size IN ($bust_size) )";
		            $filter["bust_size"] = $this->input->post('checkbox_bust_size');
		        }
		        //Color de ojos
		        if ( ($this->input->post('checkbox_eyes_color') !== null) && $this->input->post('checkbox_eyes_color') != '') {
		            $eyes_color = "'" . implode("', '", $this->input->post('checkbox_eyes_color')) . "'";
		            $str.= " AND ( eyes_color IN ($eyes_color) )";
		            $filter["eyes_color"] = $this->input->post('checkbox_eyes_color');
		        }
		        if( strpos($str," AND") == 0){
		            $str = substr($str,4);
		        }
		        $data = $this->Welcome_model->get_profile($str, $limit, $offset);
		        $this->session->set_userdata('where', $str);
		    } else {
		        $data = $this->Welcome_model->get_all_profile($limit, $offset);
		    }
		} else {
		    $data = $this->Welcome_model->get_all_profile($limit, $offset);
		}

		$this->session->set_userdata('p_offset',  $limit);
		  
		$myseo = $this->Welcome_model->myseo();


		foreach ($data as &$value) {
			$value->seo = strtolower( strtr( $this->Welcome_model->get_title($value->id), $this->unwanted_array ) );
		}

		// sidebar_new
		//COLOR DE PIEL
	    if ( ($this->input->post('checkbox_figure') !== null) && $this->input->post('checkbox_figure') != '') {
	        $figure = $this->input->post('checkbox_figure');
	    }
	    //COLOR DE PELO
	    if ( ($this->input->post('checkbox_hair_color') !== null) && $this->input->post('checkbox_hair_color') != '') {
	        $hair_color = $this->input->post('checkbox_hair_color');
	    }
	    //RANGO DE EDAD
	    if ( ($this->input->post('age1') !== null) && $this->input->post('age1') != '') {
	        $age1 = $this->input->post('age1');
	    } else {
	        $age1 = 18;
	    }
	    if ( ($this->input->post('age2') !== null) && $this->input->post('age2') != '') {
	        $age2 = $this->input->post('age2');
	    } else {
	        $age2 = 40;
	    }
	    
	    //RANGO DE PRECIOS
	    if ( ($this->input->post('amount1') !== null) && $this->input->post('amount1') != '') {
	        $amount1 = $this->input->post('amount1');
	    } else {
	        $amount1 = 180000;
	    }

	    if ( ($this->input->post('amount2') !== null) && $this->input->post('amount2') != '') {
	        $amount2 = $this->input->post('amount2');
	    } else {
	        $amount2 = 600000;
	    }
	    
	    //CONTEXTURA
	    if ( ($this->input->post('checkbox_but_size') !== null) && $this->input->post('checkbox_but_size') != '') {
	        $but_size = $this->input->post('checkbox_but_size');
	    }
	    //TAMAÑO DE COLA
	    if ( ($this->input->post('checkbox_cola_size') !== null) && $this->input->post('checkbox_cola_size') != '') {
	        $bust_size = $this->input->post('checkbox_cola_size');
	    }
	    //TAMAÑO DEL BUSTO
	    if ( ($this->input->post('checkbox_bust_size') !== null) && $this->input->post('checkbox_bust_size') != '') {
	        $bust_size = $this->input->post('checkbox_bust_size');
	    }
	    //COLOR DE OJOS
	    if ( ($this->input->post('checkbox_eyes_color') !== null) && $this->input->post('checkbox_eyes_color') != '') {
	        $eyes_color = $this->input->post('checkbox_eyes_color');
	    }	    
	    // END sidebar_new

		$filters['figure'] = new StdClass;
		$filters['figure']->Blanca = $this->Welcome_model->isInArray($filter["figure"],"Blanca");
		$filters['figure']->Morena = $this->Welcome_model->isInArray($filter["figure"],"Morena");
		$filters['figure']->Triguena = $this->Welcome_model->isInArray($filter["figure"],"Triguena");

		$filters['hair_color'] = new StdClass;
		$filters['hair_color']->Rubio = $this->Welcome_model->isInArray($filter["hair_color"],"Rubio");
		$filters['hair_color']->Rojo = $this->Welcome_model->isInArray($filter["hair_color"],"Rojo");
		$filters['hair_color']->Negro = $this->Welcome_model->isInArray($filter["hair_color"],"Negro");
		$filters['hair_color']->Castano = $this->Welcome_model->isInArray($filter["hair_color"],"Castaño");

		$filters['bust_size'] = new StdClass;
		$filters['bust_size']->A = $this->Welcome_model->isInArray($filter["bust_size"],"A");
		$filters['bust_size']->B = $this->Welcome_model->isInArray($filter["bust_size"],"B");
		$filters['bust_size']->C = $this->Welcome_model->isInArray($filter["bust_size"],"C");
		$filters['bust_size']->D = $this->Welcome_model->isInArray($filter["bust_size"],"D+");

		$filters['but_size'] = new StdClass;
		$filters['but_size']->Normal = $this->Welcome_model->isInArray($filter["but_size"],"Normal");
		$filters['but_size']->Delgada = $this->Welcome_model->isInArray($filter["but_size"],"Delgada");
		$filters['but_size']->Grande = $this->Welcome_model->isInArray($filter["but_size"],"Grande");
		$filters['but_size']->Atletica = $this->Welcome_model->isInArray($filter["but_size"],"Atletica");

		$filters['eyes_color'] = new StdClass;
		$filters['eyes_color']->Azules = $this->Welcome_model->isInArray($filter["eyes_color"],"Azules");
		$filters['eyes_color']->Cafes = $this->Welcome_model->isInArray($filter["eyes_color"],"Cafes");
		$filters['eyes_color']->Verdes = $this->Welcome_model->isInArray($filter["eyes_color"],"Verdes");
		$filters['eyes_color']->Oscuros = $this->Welcome_model->isInArray($filter["eyes_color"],"Oscuros");
		$filters['eyes_color']->Miel = $this->Welcome_model->isInArray($filter["eyes_color"],"Miel");

		$filters['occupation'] = new StdClass;
		$filters['occupation']->Delgada = $this->Welcome_model->isInArray($filter["occupation"],"Delgada");
		$filters['occupation']->Normal = $this->Welcome_model->isInArray($filter["occupation"],"Normal");
		$filters['occupation']->Grande = $this->Welcome_model->isInArray($filter["occupation"],"Grande");
		$filters['occupation']->Atletica = $this->Welcome_model->isInArray($filter["occupation"],"Atletica");

		$views['sidebar_new'] = $this->load->view('sidebar_new', NULL, TRUE);
    	$this->load->view('welcome', array(	'data' 	  => $data,
    										'fb'	  => $fb,
    										'tw'	  => $tw,
    										'wa'	  => $wa,
    										'filters' => $filters,
    										'myseo'	  => $myseo,
    										'mobile'  => $this->Welcome_model->isMobile()), 
    								$views);
    }


    function details($seo) {
    	$actual_link = base_url() . KEY_WORD . $seo;
      	$fb = $actual_link;
      	$tw = "El mejor catálogo de escorts en Bogotá, fotos 100% reales con servicio garantizado. ";
      	$wa = "El mejor catálogo de escorts en Bogotá, fotos 100% reales con servicio garantizado. " . $actual_link;

      	$id = $this->Welcome_model->getIdBySeo($seo);
      	$this->Welcome_model->update_view($id);

		$data = $this->Welcome_model->get_user_profile($id);
  		$userid = $data->userid;
  		$week =  $this->Welcome_model->sqlGetWeekSchedule($userid);
  		$weekday = array();
  		$key = 0;
  		
		foreach ($week as $value) {
			$weekday[$key] = new StdClass;
		    $weekday[$key]->day = $this->Welcome_model->explodeTimeslot($value);
		    $weekday[$key]->dayinSpanish = $this->Welcome_model->GetDayInSpanish(date('l', strtotime("+".$key." days")));
		    $key++;
		}

  		$last_update = $this->Welcome_model->sqlLastAgendaUpdate($userid);
		    
  		$images = explode(",",$data->pictures);
  		$i = 0;
      	$this->load->view('details', array(	'actual_link'	=> $actual_link,
      										'images'		=> $images, 
      										'i'				=> $i,
      										'data'			=> $data,
      										'tw'			=> $tw,
      										'wa'			=> $wa,
      										'fb'			=> $fb,
      										'last_update' 	=> $last_update,
      										'weekday' 		=> $weekday));
    }


    function jobs() {
    	$wa = "El%20mejor%20catálogo%20de%20escorts%20en%20Bogotá,%20fotos%20100%%20reales%20con%20servicio%20garantizado%20https://www.lacelestina.co";
    	$this->load->view('jobs', array('wa' => $wa));
    }


    function ajax_lazyload() {
		if($this->Welcome_model->isMobile()){
		    $limit = NUMBER_OF_PROFILES_SCROLL_MOBILE;
		} else $limit = NUMBER_OF_PROFILES_SCROLL_DESKTOP;

		if ($this->session->where != '') {
			$data = $this->Welcome_model->get_profile($this->session->where, $limit, $this->session->p_offset);
		} else {
			$data = $this->Welcome_model->get_all_profile($limit, $this->session->p_offset);
		}
		$this->session->set_userdata('p_offset', $this->session->p_offset + $limit);

		foreach ($data as &$value) {
			$value->seo = strtolower( strtr( $this->Welcome_model->get_title($value->id), $this->unwanted_array ) );
		}

		$str = "";

		if (count($data)) {
		    foreach ($data as $rows) {
		        $imgaes = explode(',', $rows->pictures);
		        $str.="<li><div class='details'>";
		        $str.="<a href='";
		        $str.= base_url() . KEY_WORD . $rows->seo;
		        $str.="'>";
		        $str.= "<img src='";
				$url = ASSETS_FOLDER . "upload/thumbs/" . $imgaes[0];
				$parsed = parse_url($url);
				if (isset($parsed['scheme']) && strtolower($parsed['scheme']) == 'https') {
				  	$url = 'http://'.substr($url,8);
				}
								
		        if (strlen($imgaes[0]) > 0) {
		            if (@getimagesize($url)) {
		                $str.= ASSETS_FOLDER . "upload/thumbs/" . $imgaes[0];
		            } else {
		                $str.= ASSETS_FOLDER . "upload/thumbs/default-ava.jpg";
		            }
		        } else {
		            $str.= ASSETS_FOLDER . "upload/thumbs/default-ava.jpg";
		        }
		        $str.="' style='height: 275px; filter: brightness(90%); -webkit-filter: brightness(90%); -moz-filter: brightness(90%); -o-filter: brightness(90%); -ms-filter: brightness(90%);' alt='".$rows->twitter."'>";

		        $str.="</a>";
		        $str.="<div class='info'  style='color:#FFCCCC;'><h3>".$rows->title."</h3><p>".number_format($rows->rate)." $/h</p></div>";

		        $str.="</div></li>";
		    }
		} 

		echo $str;

	}


	function category($cat, $cat_value) {
	  	ini_set('display_startup_errors',1);
	  	ini_set('display_errors',1);
	  	error_reporting(-1);
	  	$fb = "https://www.lacelestina.co";
	  	$tw = "El mejor catálogo de escorts en Bogotá y Medellin, fotos 100% reales con servicio garantizado ";
	  	$wa = "El%20mejor%20catálogo%20de%20escorts%20en%20Bogotá,%20fotos%20100%%20reales%20con%20servicio%20garantizado%20https://www.lacelestina.co";
	  	$filter = [
	    	"sex" => "",
	    	"sexuality" => "",
	    	"occupation" => "",
	    	"bust_size" => "",
	    	"but_size" => "",
	    	"rate" => "",
	    	"age1" => "",
	    	"age2" => "",
	    	"figure" => "",
	    	"amount1" => "",
	    	"amount2" => "",
	    	"eyes_color" => "",
	    	"hair_color" => ""
	  	];

	  	if ($this->Welcome_model->isMobile()){
	    	$limit = NUMBER_OF_PROFILES_ONLOAD_MOBILE;
	  	} else $limit = NUMBER_OF_PROFILES_ONLOAD_DESKTOP;
	  
	  	if ($cat_value == 'D') {
	  		$cat_value = 'D+';
	  	}
	  	$cat_name= "";
	  	$cate = [
	  		'color-de-piel' 	=> 'figure',
	  		'color-de-pelo' 	=> 'hair_color',
	  		'tamano-de-busto' 	=> 'Bust_Size',
	  		'contextura' 		=> 'But_Size',
	  		'color-de-ojos' 	=> 'eyes_color',
	  		'tamano-de-cola' 	=> 'occupation'
		];
		$str = "";

		foreach ($cate as $key => $value) {
			if ($cat==$key){
				$str = $value. " LIKE '".$cat_value."'";
				$cat_name = $value;
			}
		} 

		$offset2 = 0;
		if ($str != "") {
			$data = $this->Welcome_model->get_profile($str, $limit, $offset2);
		} else {
			$data = $this->Welcome_model->get_all_profile($limit, $offset2);
		}
	  	
		$this->session->set_userdata('p_offset2', $limit);
	  	$myseo = $this->Welcome_model->myseo();

		foreach ($data as &$value) {
			$value->seo = strtolower( strtr( $this->Welcome_model->get_title($value->id), $this->unwanted_array ) );
		}

		// sidebar_new
		//COLOR DE PIEL
	    if ( ($this->input->post('checkbox_figure') !== null) && $this->input->post('checkbox_figure') != '') {
	        $figure = $this->input->post('checkbox_figure');
	    }
	    //COLOR DE PELO
	    if ( ($this->input->post('checkbox_hair_color') !== null) && $this->input->post('checkbox_hair_color') != '') {
	        $hair_color = $this->input->post('checkbox_hair_color');
	    }
	    //RANGO DE EDAD
	    if ( ($this->input->post('age1') !== null) && $this->input->post('age1') != '') {
	        $age1 = $this->input->post('age1');
	    } else {
	        $age1 = 18;
	    }
	    if ( ($this->input->post('age2') !== null) && $this->input->post('age2') != '') {
	        $age2 = $this->input->post('age2');
	    } else {
	        $age2 = 40;
	    }
	    
	    //RANGO DE PRECIOS
	    if ( ($this->input->post('amount1') !== null) && $this->input->post('amount1') != '') {
	        $amount1 = $this->input->post('amount1');
	    } else {
	        $amount1 = 180000;
	    }

	    if ( ($this->input->post('amount2') !== null) && $this->input->post('amount2') != '') {
	        $amount2 = $this->input->post('amount2');
	    } else {
	        $amount2 = 600000;
	    }
	    
	    //CONTEXTURA
	    if ( ($this->input->post('checkbox_but_size') !== null) && $this->input->post('checkbox_but_size') != '') {
	        $but_size = $this->input->post('checkbox_but_size');
	    }
	    //TAMAÑO DE COLA
	    if ( ($this->input->post('checkbox_cola_size') !== null) && $this->input->post('checkbox_cola_size') != '') {
	        $bust_size = $this->input->post('checkbox_cola_size');
	    }
	    //TAMAÑO DEL BUSTO
	    if ( ($this->input->post('checkbox_bust_size') !== null) && $this->input->post('checkbox_bust_size') != '') {
	        $bust_size = $this->input->post('checkbox_bust_size');
	    }
	    //COLOR DE OJOS
	    if ( ($this->input->post('checkbox_eyes_color') !== null) && $this->input->post('checkbox_eyes_color') != '') {
	        $eyes_color = $this->input->post('checkbox_eyes_color');
	    }	    
	    // END sidebar_new

		$filters['figure'] = new StdClass;
		$filters['figure']->Blanca = $this->Welcome_model->isInArray($filter["figure"],"Blanca");
		$filters['figure']->Morena = $this->Welcome_model->isInArray($filter["figure"],"Morena");
		$filters['figure']->Triguena = $this->Welcome_model->isInArray($filter["figure"],"Triguena");

		$filters['hair_color'] = new StdClass;
		$filters['hair_color']->Rubio = $this->Welcome_model->isInArray($filter["hair_color"],"Rubio");
		$filters['hair_color']->Rojo = $this->Welcome_model->isInArray($filter["hair_color"],"Rojo");
		$filters['hair_color']->Negro = $this->Welcome_model->isInArray($filter["hair_color"],"Negro");
		$filters['hair_color']->Castano = $this->Welcome_model->isInArray($filter["hair_color"],"Castaño");

		$filters['bust_size'] = new StdClass;
		$filters['bust_size']->A = $this->Welcome_model->isInArray($filter["bust_size"],"A");
		$filters['bust_size']->B = $this->Welcome_model->isInArray($filter["bust_size"],"B");
		$filters['bust_size']->C = $this->Welcome_model->isInArray($filter["bust_size"],"C");
		$filters['bust_size']->D = $this->Welcome_model->isInArray($filter["bust_size"],"D+");

		$filters['but_size'] = new StdClass;
		$filters['but_size']->Normal = $this->Welcome_model->isInArray($filter["but_size"],"Normal");
		$filters['but_size']->Delgada = $this->Welcome_model->isInArray($filter["but_size"],"Delgada");
		$filters['but_size']->Grande = $this->Welcome_model->isInArray($filter["but_size"],"Grande");
		$filters['but_size']->Atletica = $this->Welcome_model->isInArray($filter["but_size"],"Atletica");

		$filters['eyes_color'] = new StdClass;
		$filters['eyes_color']->Azules = $this->Welcome_model->isInArray($filter["eyes_color"],"Azules");
		$filters['eyes_color']->Cafes = $this->Welcome_model->isInArray($filter["eyes_color"],"Cafes");
		$filters['eyes_color']->Verdes = $this->Welcome_model->isInArray($filter["eyes_color"],"Verdes");
		$filters['eyes_color']->Oscuros = $this->Welcome_model->isInArray($filter["eyes_color"],"Oscuros");
		$filters['eyes_color']->Miel = $this->Welcome_model->isInArray($filter["eyes_color"],"Miel");

		$filters['occupation'] = new StdClass;
		$filters['occupation']->Delgada = $this->Welcome_model->isInArray($filter["occupation"],"Delgada");
		$filters['occupation']->Normal = $this->Welcome_model->isInArray($filter["occupation"],"Normal");
		$filters['occupation']->Grande = $this->Welcome_model->isInArray($filter["occupation"],"Grande");
		$filters['occupation']->Atletica = $this->Welcome_model->isInArray($filter["occupation"],"Atletica");

		$views['sidebar_new'] = $this->load->view('sidebar_new', NULL, TRUE);
    	$this->load->view('category', array(	'data' 	  	=> $data,
	    										'fb'	  	=> $fb,
	    										'tw'	  	=> $tw,
	    										'wa'	  	=> $wa,
	    										'filters' 	=> $filters,
	    										'myseo'	  	=> $myseo,
	    										'cat_value' => $cat_value,
	    										'cat_name'  => $cat_name,
	    										'mobile'  => $this->Welcome_model->isMobile()), 
    										$views);

	}


	function ajax_lazyload2($cat_name, $cat_value) {
		if($this->Welcome_model->isMobile()){
		    $limit = NUMBER_OF_PROFILES_SCROLL_MOBILE;
		} else $limit = NUMBER_OF_PROFILES_SCROLL_DESKTOP;

		$where = "";
		if($cat_name !== '' && $cat_name !== null && $cat_value !== '' && $cat_value !== null ) {
    		$where = $cat_name." LIKE '".$cat_value."'";
		}
		if ($where !== '') {
			$data = $this->Welcome_model->get_profile($where, $limit, $this->session->p_offset2);
			$this->session->set_userdata('p_offset2', $this->session->p_offset2 + $limit);

			$str = "";

			if (count($data)) {
				foreach ($data as &$value) {
					$value->seo = strtolower( strtr( $this->Welcome_model->get_title($value->id), $this->unwanted_array ) );
				}

			    foreach ($data as $rows) {
			        $imgaes = explode(',', $rows->pictures);
			        $str.="<li><div class='details'>";
			        $str.="<a href='";
			        $str.= base_url() . KEY_WORD . $rows->seo;
			        $str.="'>";
			        $str.= "<img src='";
			        $url = ASSETS_FOLDER . "upload/thumbs/" . $imgaes[0];
					$parsed = parse_url($url);
					if (isset($parsed['scheme']) && strtolower($parsed['scheme']) == 'https') {
					  	$url = 'http://'.substr($url,8);
					}
			        if (strlen($imgaes[0]) > 0) {
			            if (@getimagesize($url)) {
			                $str.= ASSETS_FOLDER . "upload/thumbs/" . $imgaes[0];
			            } else {
			                $str.= ASSETS_FOLDER . "upload/thumbs/default-ava.jpg";
			            }
			        } else {
			            $str.= ASSETS_FOLDER . "upload/thumbs/default-ava.jpg";
			        }
			        $str.="' style='height: 275px; filter: brightness(90%); -webkit-filter: brightness(90%); -moz-filter: brightness(90%); -o-filter: brightness(90%); -ms-filter: brightness(90%);' alt='".$rows->twitter."'>";

			        $str.="</a>";
			        $str.="<div class='info'  style='color:#FFCCCC;'><h3>".$rows->title."</h3><p>".number_format($rows->rate)." $/h</p></div>";

			        $str.="</div></li>";
			    }
			} 
			echo $str;
		}
	}

}