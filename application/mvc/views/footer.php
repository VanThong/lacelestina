

<script src="<?php echo ASSETS_FOLDER; ?>bootstrap/js/jquery.min.js"></script> 
<script src="<?php echo ASSETS_FOLDER; ?>bootstrap/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo ASSETS_FOLDER; ?>bootstrap/js/boodigo_widget.js"> </script>
<script>
$(document).ready(function () {
  var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {          
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
  
  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
  });  
});
</script>




<!-- Bootstrap core JavaScript



    ================================================== -->



    <!-- Placed at the end of the document so the pages load faster -->

<script src="<?php echo ASSETS_FOLDER; ?>js/classie.js"></script>

<script src="<?php echo ASSETS_FOLDER; ?>js/main.js"></script>

<script src="<?php echo ASSETS_FOLDER; ?>bootstrap/js/jquery-1.10.2.min.js"></script>

<script src="<?php echo ASSETS_FOLDER; ?>js/bootstrap.min.js"></script>

<script src="<?php echo ASSETS_FOLDER; ?>bootstrap/js/jasny-bootstrap.min.js"></script>

    

<link rel="stylesheet" href="<?php echo ASSETS_FOLDER; ?>bootstrap/css/jquery-ui.css">

<script src="<?php echo ASSETS_FOLDER; ?>bootstrap/js/jquery-1.10.2.js"></script>

<script src="<?php echo ASSETS_FOLDER; ?>bootstrap/js/jquery-ui.js"></script>
<script src="<?php echo ASSETS_FOLDER; ?>bootstrap/js/jquery.ui.touch-punch.min.js"></script>

<!-- This Function is used for Price -->  

<script>
          $(function() {
          $( "#slider-range" ).slider({
            range: true,
            min: 80000,
            max: 500000,
            values: [ 80000, 500000 ],
            step: 10000,
            slide: function( event, ui ) {
              $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
			  $( "#amount1" ).val(  ui.values[ 0 ] );
		      $( "#amount2" ).val(  ui.values[ 1 ] );
            }
          });
          $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
            " - $" + $( "#slider-range" ).slider( "values", 1 ) );
        });
        </script>

<!--<script>
//$(function() {
//	
//	$( "#slider-range" ).blur(function(){
//    alert("This input field has lost its focus.");
//});
//    $( "#slider-range" ).slider({
//      range: true,
//      min: 0,
//      max: 300,
//      values: [ 0, 300 ],
//      slide: function( event, ui ) {
//        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
//		$( "#amount1" ).val(  ui.values[ 0 ] );
//		$( "#amount2" ).val(  ui.values[ 1 ] );
//      }
//    });
//    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
//      " - $" + $( "#slider-range" ).slider( "values", 1 ) );
//  });
//  
  </script> -->
<!-- This Function is used for Age-->

  
<script>
  $(function() {

    $( "#slider-range-age" ).slider({

      range: true,

      min: 18,

      max: 40,

      values: [ 18, 40 ],

      slide: function( event, ui ) {

        $( "#age" ).val( "" + ui.values[ 0 ] + " - " + ui.values[ 1 ] );

		$( "#age1" ).val(  ui.values[ 0 ] );

		$( "#age2" ).val(  ui.values[ 1 ] );

      }

    });

    $( "#age" ).val( "" + $( "#slider-range-age" ).slider( "values", 0 ) +

      " - " + $( "#slider-range-age" ).slider( "values", 1 ) );

  });

  </script>
 

  </body>

</html>