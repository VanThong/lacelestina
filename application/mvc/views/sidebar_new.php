<div class="color-options">
	<h2>Color de piel:</h2>
	<p>
		<input id="checkbox_Thin" type="checkbox" <?php if ( $filters["figure"]->Blanca ) echo " checked = 'checked' ";?> value="Blanca"  name="checkbox_figure[]" > Blanca </input>
	</p>
	<p>
		<input id="checkbox_Slim" type="checkbox" <?php if ( $filters["figure"]->Morena ) echo " checked = 'checked' ";?> value="Morena"  name="checkbox_figure[]" > Morena </input>
	</p>
	<p>
		<input id="checkbox_Sporty" type="checkbox" <?php if ( $filters["figure"]->Triguena ) echo " checked = 'checked' ";?> value="Triguena"  name="checkbox_figure[]" > Trigueña </input>
	</p>
</div>
<div class="color-options">
	<h2>Color de pelo:</h2>
	<p>
		<input id="checkbox_Blonde" type="checkbox" <?php if ( $filters["hair_color"]->Rubio ) echo " checked = 'checked' ";?> value="Rubio" name="checkbox_hair_color[]"> Rubio </input>
	</p>
	<p>
		<input id="checkbox_Red" type="checkbox" <?php if ( $filters["hair_color"]->Rojo ) echo " checked = 'checked' ";?> value="Rojo" name="checkbox_hair_color[]"> Rojo </input>
	</p>
	<p>
		<input id="checkbox_Blondeblack" type="checkbox" <?php if ( $filters["hair_color"]->Negro ) echo " checked = 'checked' ";?> value="Negro" name="checkbox_hair_color[]"> Negro </input>
	</p>
	<p>
		<input id="checkbox_Castano" type="checkbox" <?php if ( $filters["hair_color"]->Castano ) echo " checked = 'checked' ";?> value="Castaño" name="checkbox_hair_color[]"> Castaño </input>
	</p>
</div>
<div class="color-options">
	<h2>Tamaño de busto :</h2>
	<p>
		<input id="checkbox_A" type="checkbox" <?php if ( $filters["bust_size"]->A ) echo " checked = 'checked' ";?> value="A" name="checkbox_bust_size[]" > A </input>
	</p>
	<p>
		<input id="checkbox_B" type="checkbox" <?php if ( $filters["bust_size"]->B ) echo " checked = 'checked' ";?> value="B" name="checkbox_bust_size[]" > B </input>
	</p>
	<p>
		<input id="checkbox_C" type="checkbox" <?php if ( $filters["bust_size"]->C ) echo " checked = 'checked' ";?>  value="C" name="checkbox_bust_size[]" > C </input>
	</p>
	<p>
		<input id="checkbox_D+" type="checkbox" <?php if ( $filters["bust_size"]->D ) echo " checked = 'checked' ";?> value="D+" name="checkbox_bust_size[]" > D+ </input>
	</p>
</div>
<div class="color-options">
	<h2>Contextura:</h2>
	<p>
		<input id="checkbox_normal" type="checkbox" <?php if ( $filters["but_size"]->Normal ) echo " checked = 'checked' ";?> value="Normal" name="checkbox_but_size[]"> Normal </input>
	</p>
	<p>
		<input id="checkbox_slim" type="checkbox" <?php if ( $filters["but_size"]->Delgada ) echo " checked = 'checked' ";?> value="Delgada" name="checkbox_but_size[]"> Delgada </input>
	</p>
	<p>
		<input id="checkbox_big" type="checkbox" <?php if ( $filters["but_size"]->Grande ) echo " checked = 'checked' ";?> value="Grande" name="checkbox_but_size[]"> Grande </input>
	</p>
	<p>
		<input id="checkbox_huge" type="checkbox" <?php if ( $filters["but_size"]->Atletica ) echo " checked = 'checked' ";?> value="Atletica" name="checkbox_but_size[]"> Atlética </input>
	</p>
</div>
<div class="color-options">
	<h2>Color de ojos:</h2>
	<p>
		<input id="checkbox_Blue" type="checkbox" <?php if ( $filters["eyes_color"]->Azules ) echo " checked = 'checked' ";?> value="Azules" name="checkbox_eyes_color[]"> Azules </input>
	</p>
	<p>
		<input id="checkbox_Brown" type="checkbox" <?php if ( $filters["eyes_color"]->Cafes ) echo " checked = 'checked' ";?> value="Cafes" name="checkbox_eyes_color[]"> Cafés </input>
	</p>
	<p>
		<input id="checkbox_Green" type="checkbox" <?php if ( $filters["eyes_color"]->Verdes ) echo " checked = 'checked' ";?> value="Verdes" name="checkbox_eyes_color[]"> Verdes </input>
	</p>
	<p>
		<input id="checkbox_Gray" type="checkbox" <?php if ( $filters["eyes_color"]->Oscuros ) echo " checked = 'checked' ";?> value="Oscuros" name="checkbox_eyes_color[]"> Oscuros </input>
	</p>
	<p>
		<input id="checkbox_Honey_black" type="checkbox" <?php if ( $filters["eyes_color"]->Miel ) echo " checked = 'checked' ";?> value="Miel" name="checkbox_eyes_color[]"> Miel </input>
	</p>
</div>
<div class="color-options">
	<h2>Tamaño de cola:</h2>
	<p>
		<input  id="checkbox_slim_a" type="checkbox" <?php if ( $filters["occupation"]->Delgada ) echo " checked = 'checked' ";?> value="Delgada" name="checkbox_cola_size[]" > Delgada </input>
	</p>
	<p>
		<input id="checkbox_normal_a" type="checkbox" <?php if ( $filters["occupation"]->Normal ) echo " checked = 'checked' ";?> value="Normal" name="checkbox_cola_size[]" > Normal </input>
	</p>
	<p>
		<input id="checkbox_big_a" type="checkbox" <?php if ( $filters["occupation"]->Grande ) echo " checked = 'checked' ";?> value="Grande" name="checkbox_cola_size[]" > Grande </input>
	</p>
	<p>
		<input id="checkbox_huge_a" type="checkbox" <?php if ( $filters["occupation"]->Atletica ) echo " checked = 'checked' ";?> value="Brasileña" name="checkbox_cola_size[]" > Brasileña </input>
	</p>
</div>
<div class="color-options" style="background:#f4f4f4; text-align:center; margin-top:50px;">
	<input type="submit" class="apply-btn" value="Aplicar"/>
	<a href="#" id="reset" class="apply-btn" style="background:#771dc3;">RESET</a>
</div>            
<script src="<?php echo ASSETS_FOLDER; ?>js/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#reset').click(function () {
			$('input:checkbox').removeAttr('checked');
		})
	})
</script>