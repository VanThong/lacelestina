<?php 
if (!defined('BASEPATH'))
exit('No direct script access allowed');
  
class Details extends CI_Controller {
  
    function __construct() {
        parent::__construct();
        $this->load->model('Main_model');
    }
    public $unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', ' '=>'-', ','=>'_' );

    public $US_STATES = array(
	    'AL'=>'Alabama',
	    'AK'=>'Alaska',
	    'AZ'=>'Arizona',
	    'AR'=>'Arkansas',
	    'CA'=>'California',
	    'CO'=>'Colorado',
	    'CT'=>'Connecticut',
	    'DE'=>'Delaware',
	    'DC'=>'District of Columbia',
	    'FL'=>'Florida',
	    'GA'=>'Georgia',
	    'HI'=>'Hawaii',
	    'ID'=>'Idaho',
	    'IL'=>'Illinois',
	    'IN'=>'Indiana',
	    'IA'=>'Iowa',
	    'KS'=>'Kansas',
	    'KY'=>'Kentucky',
	    'LA'=>'Louisiana',
	    'ME'=>'Maine',
	    'MD'=>'Maryland',
	    'MA'=>'Massachusetts',
	    'MI'=>'Michigan',
	    'MN'=>'Minnesota',
	    'MS'=>'Mississippi',
	    'MO'=>'Missouri',
	    'MT'=>'Montana',
	    'NE'=>'Nebraska',
	    'NV'=>'Nevada',
	    'NH'=>'New Hampshire',
	    'NJ'=>'New Jersey',
	    'NM'=>'New Mexico',
	    'NY'=>'New York',
	    'NC'=>'North Carolina',
	    'ND'=>'North Dakota',
	    'OH'=>'Ohio',
	    'OK'=>'Oklahoma',
	    'OR'=>'Oregon',
	    'PA'=>'Pennsylvania',
	    'RI'=>'Rhode Island',
	    'SC'=>'South Carolina',
	    'SD'=>'South Dakota',
	    'TN'=>'Tennessee',
	    'TX'=>'Texas',
	    'UT'=>'Utah',
	    'VT'=>'Vermont',
	    'VA'=>'Virginia',
	    'WA'=>'Washington',
	    'WV'=>'West Virginia',
	    'WI'=>'Wisconsin',
	    'WY'=>'Wyoming',
	);


    function index($seo) {
    	$actual_link = base_url() . KEY_WORD . $seo;
      	$fb = $actual_link;
      	$tw = "El mejor catálogo de escorts en Bogotá, fotos 100% reales con servicio garantizado. ";
      	$wa = "El mejor catálogo de escorts en Bogotá, fotos 100% reales con servicio garantizado. " . $actual_link;

      	$id = $this->Main_model->getIdBySeo($seo);
      	$this->Main_model->update_view($id);

		$data = $this->Main_model->get_user_profile($id);
  		$userid = $data->userid;
  		$week =  $this->Main_model->sqlGetWeekSchedule($userid);
  		$weekday = array();
  		$key = 0;
  		
		foreach ($week as $value) {
			$weekday[$key] = new StdClass;
		    $weekday[$key]->day = $this->Main_model->explodeTimeslot($value);
		    $weekday[$key]->dayinSpanish = $this->Main_model->GetDayInSpanish(date('l', strtotime("+".$key." days")));
		    $key++;
		}

  		$last_update = $this->Main_model->sqlLastAgendaUpdate($userid);
		    
  		$images = explode(",",$data->pictures);
  		$i = 0;
      	$this->load->view('details', array(	'actual_link'	=> $actual_link,
      										'images'		=> $images, 
      										'i'				=> $i,
      										'data'			=> $data,
      										'tw'			=> $tw,
      										'wa'			=> $wa,
      										'fb'			=> $fb,
      										'last_update' 	=> $last_update,
      										'weekday' 		=> $weekday));
    }


}