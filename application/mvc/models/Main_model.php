<?php
class Main_model extends CI_Model {
	
	public function get_profile($str, $limit, $offset) {
        return $this->db->order_by('ordernum', 'asc')->limit($limit, $offset)->get_where('lam_user_profile', $str)->result();
    }


    public function get_all_profile($limit, $offset) {
        return $this->db->order_by('ordernum', 'asc')->limit($limit, $offset)->get_where('lam_user_profile')->result();
    }


    public function isMobile() {
    	return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
	}


	public function myseo() {
		return $this->db->order_by('ordernum', 'asc')->limit(1, 0)->get_where('lam_user_profile', array('userid' => 80))->result()[0]->facebook;
	}


	public function update_view($id) {
		$this->db->set('views', 'views+1', FALSE)->where('id', $id)->update('lam_user_profile');
	}


	public function get_user_profile($id) {
        return $this->db->order_by('id', 'desc')->get_where('lam_user_profile', array('id' => $id))->result()[0];
    }


    public function sqlGetWeekSchedule($uid){
	    $sch = $this->db->query( "SELECT timeslot,date FROM lam_day_calendar WHERE userid='$uid' AND (date BETWEEN (CURDATE() - INTERVAL 1 DAY) AND (CURDATE() + INTERVAL 7 DAY) ) ORDER BY date;" )->result();

	    $temp  = [];
	    $dates = [];
	    $rel   = [];
	    $df    = [];
	    $wsch  = [];
	    
	    $df_week = $this->db->query( "SELECT monday, tuesday, wednesday, thursday, friday, saturday, sunday FROM lam_user_profile WHERE userid='$uid';" )->result();
	        
	    $today = date('N',strtotime("-5 hour", time())) - 1;

	    //$todaya = date('Y-m-d',strtotime("-0 hour", time()));
	    
	    foreach ($sch as $key => $value) {
	        array_push($temp, $value->timeslot);
	        array_push($dates, date('N', strtotime($value->date))  - 1);    
	    }

	    for ($i = 0; $i < 7; $i++) {
	        if (($i + $today) < 7) {
	        	array_push($wsch,$i + $today);
	        } else {
	        	array_push($wsch,$i + $today - 7);
	        }
	    }

	    for ($i = 0; $i < 7; $i++) {
	        if (count($dates) > 0 and in_array($wsch[$i], $dates)) {
	            array_push($rel,$temp[array_search($wsch[$i], $dates)]);   
	        } else {
	            if (($i + $today) < 7 ) {
	            	$day = $this->GetDayByIndex($i + $today);
	                array_push($rel,$df_week[0]->$day);
	            } else {
	            	$day = $this->GetDayByIndex($i + $today - 7);
	                array_push($rel,$df_week[0]->$day);
	            }
	        }
	    }
	    return $rel;
	}


	public function GetDayByIndex($day) {
	    if($day == 0)
	        return "monday";
	    else if($day == 1)
	        return "tuesday";
	    else if($day == 2)
	        return "wednesday";
	    else if($day == 3)
	        return "thursday";
	    else if($day == 4)
	        return "friday";
	    else if($day == 5)
	        return "saturday";
	    else if($day == 6)
	        return "sunday";
	}



	public function sqlLastAgendaUpdate($uid){
    
	    $res = $this->db->order_by('date_create', 'desc')->select('date_create')->get_where('lam_day_calendar', array('userid' => $uid))->result();
	    if (!empty($res)) {
		    $sdate = date('Y-m-d H:i:s',strtotime("-5 hours " . $res[0]->date_create));
		    $today = date('Y-m-d',strtotime("-5 hour", time()));
		    $yesterday = date('Y-m-d',strtotime("-29 hour", time()));

		    $ret = "";

		    if(substr($sdate,0,10) == $today)
		        $ret = "Hoy a las " . substr($sdate,10,10);   
		    else if(substr($sdate,0,10) == $yesterday)
		        $ret = "Ayer a las " . substr($sdate,10,10);
		    else
		        $ret = substr($sdate,0,10) . " a las " . substr($sdate,10,10);   
		       
		    return $ret;
	    }
	}


	public function get_title($id) {
		return $this->db->select('title')->get_where('lam_user_profile', array('id' => $id))->result()[0]->title;
	}


	public function explodeTimeslot($day){    
	    $tl = explode(",",$day);
	    $rel ="";
	    
	    $start = -1;
	    $end = -1;
	    $counting = false;
	    
		
	    for($i=0;$i<24;$i++){
		    if($tl[$i] == "A"){
		        //echo $i;
		        if(!$counting){
		          	$start = $i;
		          	$counting = true;
		          	$rel = $rel.$this->val2tl($start)."-";
		          	//echo $start." ";
		        }
		        if($counting && $i == 23){
		          	$rel = $rel.$this->val2tl(0)." | ";
		        }     
		    } else {
		        if($counting){          
		          	$counting = false;
		          	$end = $i;
		          	$rel = $rel.$this->val2tl($end)." | ";          
		          	//echo $end." ";
		        }        
		    }
	    }
	    return substr($rel, 0, strlen($rel) - 2);
	}


	public function val2tl($val){
	    $rel = '';
	    if($val < 10 ){
	      	$rel = $rel.'0'.strval($val) . 'am';
	    }
	    if($val >= 10 and $val <= 12){
	      	$rel = $rel.strval($val) . 'am';
	    }
	    if($val > 12 and $val < 22){
	      	$rel = $rel.'0'.strval($val-12) . 'pm';
	    }
	    if($val >= 22 and $val <= 23){
	      	$rel = $rel.strval($val-12) . 'pm';
	    }
	    return $rel;
	}


	public function GetDayInSpanish($day) {
	    if($day == "Monday")
	        return "Lunes";
	    else if($day == "Tuesday")
	        return "Martes";
	    else if($day == "Wednesday")
	        return "Miércoles";
	    else if($day == "Thursday")
	        return "Jueves";
	    else if($day == "Friday")
	        return "Viernes";
	    else if($day == "Saturday")
	        return "Sábado";
	    else if($day == "Sunday")
	        return "Domingo";
	}


	public function isInArray($arr,$var) {
       	$count = 0;
       	if(is_array($arr)){
            for($i=0;$i<count($arr);$i++){
                if($arr[$i] == $var) {
                    $count ++;
                }
            } 
       	}
       	if ($count > 0) return true;
       	else return false;
    }


    public function getIdBySeo($seo) {
    	if (strpos($seo, '-') !== false) {
    		$seo = str_replace("-", " ", $seo);
    	}
    	if (strpos($seo, '_') !== false) {
    		$seo = str_replace("_", ",", $seo);
    	}
    	return $this->db->select('id')->get_where('lam_user_profile', array(strtolower(  'title' ) => $seo))->result()[0]->id;
    }
}