<style type="text/css">
	#new_foot{width:100%;display:block;margin:0 auto;clear:both;padding:30px}
	.col_foot{width:16.6666666%;display:inline-block;float:left;margin:0;list-style:none}
	.col_foot h2{color:blue}
	.title_foot{margin:0 0 50px 0;padding:0;font-size:16px;text-transform:uppercase;line-height:normal;font-weight:normal;height:25px;}
	.list_foot{margin:0;padding:0;list-style:none;display:block}
	.list_foot li{position:relative;padding-left:40px;display:block;margin-bottom:15px;padding:0 0 5px 0}
	.list_foot >li a{text-decoration:none;font-size:13px;text-transform:none;color:white}
	@media(max-width: 766px)
	{.col_foot{width:50%}}
</style>

<div id="new_foot" class="footer-top">
	<div class="col_foot">
		<h2 class="title_foot">COLOR DE PIEL</h2>
		<ul class="list_foot">
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>color-de-piel/Blanca">BLANCA</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>color-de-piel/Morena">MORENA</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>color-de-piel/Triguena">TRIGUEÑA</a>
			</li>
		</ul>
	</div>
	<div class="col_foot">
		<h2 class="title_foot">COLOR DE PELO</h2>
		<ul class="list_foot">
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>color-de-pelo/Rubio">RUBIO</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>color-de-pelo/Rojo">ROJO</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>color-de-pelo/Negro">NEGRO</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>color-de-pelo/Castano">CASTAÑO</a>
			</li>
		</ul>
	</div>
	<div class="col_foot">
		<h2 class="title_foot">TAMAÑO DE BUSTO</h2>
		<ul class="list_foot">
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>tamano-de-busto/A">A</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>tamano-de-busto/B">B</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>tamano-de-busto/C">C</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>tamano-de-busto/<?php echo urlencode('D+');?>">D+</a>
			</li>
		</ul>
	</div>
	<div class="col_foot">
		<h2 class="title_foot">CONTEXTURA</h2>
		<ul class="list_foot">
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>contextura/Normal">NORMAL</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>contextura/Delgada">DELGADA</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>contextura/Grande">GRANDE</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>contextura/Atletica">ATLÉTICA</a>
			</li>
		</ul>
	</div>
	<div class="col_foot">
		<h2 class="title_foot">COLOR DE OJOS</h2>
		<ul class="list_foot">
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>color-de-ojos/Azules">AZULES</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>color-de-ojos/Cafes">CAFÉS</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>color-de-ojos/Verdes">VERDES</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>color-de-ojos/Oscuros">OSCUROS</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>color-de-ojos/Miel">MIEL</a>
			</li>
		</ul>
	</div>
	<div class="col_foot">
		<h2 class="title_foot">TAMAÑO DE COLA</h2>
		<ul class="list_foot">
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>tamano-de-cola/Delgada">DELGADA</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>tamano-de-cola/Normal">NORMAL</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>tamano-de-cola/Grande">GRANDE</a>
			</li>
			<li>
				<a href="<?php echo CATEGORY_FOLDER; ?>tamano-de-cola/Brasilena">BRASILEÑA</a>
			</li>
		</ul>
	</div>
</div>